## About

A network visualisation for the database at http://acoustics.mpiwg-berlin.mpg.de developed by Anne Kilgus

The project is led by [Viktoria Tkaczyk](https://www.mpiwg-berlin.mpg.de/en/users/tkaczyk)

## Data source

The original data can be found here:
- https://acoustics.mpiwg-berlin.mpg.de/rest/nodes_for_relations (nodes)
- https://acoustics.mpiwg-berlin.mpg.de/rest/relation_test (edges)
